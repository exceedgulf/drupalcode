<?php

namespace Drupal\ain_iot\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining IOT Records entities.
 *
 * @ingroup ain_iot
 */
interface IotRecordInterface extends  ContentEntityInterface {

  // Add get/set methods for your configuration properties here.


  /**
   * Gets the IOT Records creation timestamp.
   *
   * @return int
   *   Creation timestamp of the IOT Records.
   */
  public function getCreatedTime();

  /**
   * Sets the IOT Records creation timestamp.
   *
   * @param int $timestamp
   *   The IOT Records creation timestamp.
   *
   * @return \Drupal\ain_iot\Entity\IotRecordInterface
   *   The called IOT Records entity.
   */
  public function setCreatedTime($timestamp);

}
