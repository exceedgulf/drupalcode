<?php

namespace Drupal\ain_gamification;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for gamification.
 */
class GamificationTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
