<?php

namespace Drupal\ain_social_login\EventSubscriber;

use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\Event\SocialAuthUserEvent;
use Drupal\social_auth\Event\SocialAuthEvents;
use Drupal\social_auth_facebook\FacebookAuthManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\user\Entity\User;

/**
 * Reacts on Social Auth events.
 */
class AinSocialAuthSubscriber implements EventSubscriberInterface {

  /**
   * Used to retrieve the token from session.
   *
   * @var \Symfony\Component\HttpFoundation\Session\SessionInterface
   */
  protected $session;

  /**
   * Used to get an instance of the SDK used by the Social Auth implementer.
   *
   * @var \Drupal\social_api\Plugin\NetworkManager
   */
  protected $networkManager;

  /**
   * The Social Auth Facebook manager.
   *
   * @var \Drupal\social_auth_facebook\FacebookAuthManager
   */
  protected $facebookManager;

  /**
   * SocialAuthSubscriber constructor.
   *
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   *   Used to retrieve the token from session.
   * @param \Drupal\social_api\Plugin\NetworkManager $network_manager
   *   Used to get an instance of the SDK used by the Social Auth implementer.
   * @param \Drupal\social_auth_facebook\FacebookAuthManager $facebook_manager
   *   The Social Auth Facebook manager.
   */
  public function __construct(SessionInterface $session, NetworkManager $network_manager, FacebookAuthManager $facebook_manager) {
    $this->session = $session;
    $this->networkManager = $network_manager;
    $this->facebookManager = $facebook_manager;
  }

  /**
   * {@inheritdoc}
   *
   * Returns an array of event names this subscriber wants to listen to.
   * For this case, we are going to subscribe for user creation and login
   * events and call the methods to react on these events.
   * 
   * we will also listen to the kernel response for redirect purposes.
   */
  public static function getSubscribedEvents() {
    $events[SocialAuthEvents::USER_CREATED] = ['onUserCreated'];
    $events[SocialAuthEvents::USER_LOGIN] = ['onUserLogin'];
    $events[KernelEvents::RESPONSE][] = ['onRespond'];

    return $events;
  }

  /**
   * Code that should be triggered onRespond event. 
   */
  public function onRespond(FilterResponseEvent $event) {
    // get the user object.
    $user = User::load(\Drupal::currentUser()->id());
    // get the user id.
    $uid = $user->uid->value;

    // exclude anonymous users and user 1 from the redirect.
    if (!$uid || $uid == "1") {
      return;
    }

    // get the user edit form url.
    $edit_page = $user->url('edit-form');
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $edit_page = '/user/' . $uid . '/edit';
    if ($language == 'ar') {
      $edit_page = '/ar' . $edit_page;
    }
    // get current url.
    $current_url = $page = \Drupal::request()->getRequestUri();

    // flag of missing fields values.
    $missing_info = FALSE;

    // define all required fields and corresponding error messages,
    // error message will be displayed if the field left empty.
    $fields = array(
      'name' => 'Please enter your mobile number.',
      'field_name' => 'Please enter your name.',
      'field_gender' => 'Please enter your gender.',
    );

    // loop through all required fields and display error message if the
    // field value was left empty.
    foreach ($fields as $field => $message) {
      if ($user->get($field)->value === NULL) {
        $missing_info = TRUE;
        drupal_set_message(new TranslatableMarkup($message), 'error', FALSE);
      }
    }

    // prevent session error messages from bieng displayed after filling all required fields.
    if (!$missing_info) {
      drupal_get_messages('error', TRUE);
    }

    // prevent redirect loop.
    if ($missing_info && !AinSocialAuthSubscriber::testUrl($current_url) && $current_url != $edit_page) {
      // redirect to user edit form if there is still at least one empty required field.
      $response = new RedirectResponse($edit_page);
      $event->setResponse($response);
    }
  }

  /**
   * Map user fields
   *
   * @param \Drupal\social_auth\Event\SocialAuthUserEvent $event
   *   The Social Auth user event object.
   */
  public function onUserCreated(SocialAuthUserEvent $event) {
    /**
     * @var Drupal\user\UserInterface $user
     *
     * For all available methods, see User class
     * @see https://api.drupal.org/api/drupal/core!modules!user!src!Entity!User.php/class/User
     */
    // If user logs in using social_auth_facebook.
    if ($event->getPluginId() == 'social_auth_facebook') {
      $fb_user = $this->facebookManager->getUserInfo();
      $user = $event->getUser();
      $pass = user_password(20);
      $user->set('field_name', $fb_user->getName());
      $user->set('field_gender', $fb_user->getGender());
      $user->setPassword($pass);
      $user->save();
    }
  }

  /**
   * Sets a drupal message when a user logs in via social_auth.
   *
   * @param \Drupal\social_auth\Event\SocialAuthUserEvent $event
   *   The Social Auth user event object.
   */
  public function onUserLogin(SocialAuthUserEvent $event) {
    switch ($event->getPluginId()) {
      case 'social_auth_facebook':
        drupal_set_message(new TranslatableMarkup('User has logged in via Facebook.'));
    }
  }

  /**
   * Check url to exclude from redirecting.
   *
   * @param string $url
   *   An array containing keywords to exclude from redirecting if were in the url.
   */
  public function testUrl($url) {
    $exclude = FALSE;
    $excluded_array = array(
      'ajax_form',
      'iframe',
    );

    foreach ($excluded_array as $value) {
      if (strpos($url, $value)) {
        $exclude = TRUE;
      }
    }

    return $exclude;
  }

}
