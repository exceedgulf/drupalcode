(function ($, _, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.view_maps = {
    attach: function (context, settings) {
      $(document).ready(function () {
        if ($('#alain-map').length) {
          var lang = $('html').attr('lang');
          var mapSettings = $(drupalSettings.ain_map.map)[0];
          mapboxgl.accessToken = mapSettings.accesstoken;

          var map = new mapboxgl.Map({
            container: 'alain-map',
            style: mapSettings.style,
            center: [55.740, 24.176],
            bearing: 90,
            zoom: 16.2,
            minZoom: 16
          });

          // Add zoom and rotation controls to the map.
          map.addControl(new mapboxgl.NavigationControl());
          // Add full screen controll.
          map.addControl(new mapboxgl.FullscreenControl());

          var nid = $('#alain-map').attr('nid');
          var contentType = ['experience', 'animal', 'attraction']

          contentType.forEach(function (type) {
            var path = drupalSettings.path.baseUrl + 'view-fields/visit-plan-' + type + '/' + nid + '/' + lang;
            $.getJSON(path, function (data) {
              data.features.forEach(function (marker) {
                // create a DOM element for the marker
                var icon = document.createElement('div');
                icon.className = 'marker ' + marker.properties.id;
                icon.style.backgroundImage = 'url(' + marker.properties.icon + ')';
                icon.style.width = 50 + 'px';
                icon.style.height = 50 + 'px';


                var language = '';
                if (lang == 'ar') {
                  language = '/ar';
                }

                var explore = '';
                if (marker.properties.type != 'venue') {
                  explore = '<div class="map-more-wrapper"><a class="map-more" href="' + language + '/node/' + marker.properties.id + '">' + Drupal.t('Explore') + '</a></div>'
                }

                var popup = new mapboxgl.Popup()
                  .setHTML(
                  '<div class="map-popup">' +
                  '<div class="col-sm-8">' +
                  '<h2><a href="' + marker.properties.path + '">' + marker.properties.name + '</a></h2>' +
                  '<div class="map-details">' + marker.properties.body + '</div>' +
                  explore +
                  '</div>' +
                  '<div class="col-sm-4"><img src="' + marker.properties.thumbnail + '"/></div>' +
                  '</div>');

                // Add marker to map
                var current = new mapboxgl.Marker(icon)
                  .setLngLat(marker.geometry.coordinates)
                  .setPopup(popup)
                  .addTo(map);

                icon.addEventListener('click', function () {
                  map.flyTo({ center: marker.geometry.coordinates, zoom: 17.5, speed: 0.4, curve: 1 });
                });
              });
            });
          })
        }
      });
    }
  };


}
)(window.jQuery, window._, window.Drupal, window.drupalSettings, window.localStorage);
