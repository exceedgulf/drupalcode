(function ($, _, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.maps = {
    attach: function (context, settings) {

    }
  };

  var searchArray = new Array();
  var count = 0;

  $(document).ready(function () {
    $(".map-form legend").click(function () {
      if ($(window).width() < 768) {
        if ($(this).parent('.fieldgroup').hasClass('open')) {
          $(this).parent('.fieldgroup').removeClass('open');
        } else {
          $(".map-form .fieldgroup").removeClass('open');
          $(this).parent('.fieldgroup').addClass('open');
        }
      }
      else {
        $(".map-form .fieldgroup").removeClass('open');
      }
    });
    
    var lang = $('html').attr('lang');
    var mapSettings = $(drupalSettings.ain_map.map)[0];
    mapboxgl.accessToken = mapSettings.accesstoken;

    var map = new mapboxgl.Map({
      container: 'alain-map',
      style: mapSettings.style,
      center: [55.740, 24.176],
      bearing: 90,
      zoom: 16.2,
      minZoom: 16,
      maxZoom: 18
    });

    // Add zoom and rotation controls to the map.
    map.addControl(new mapboxgl.NavigationControl());
    // Add full screen controll.
    map.addControl(new mapboxgl.FullscreenControl());

    // TODO divide the API to events and the rest of events.
    var path = drupalSettings.path.baseUrl + 'api/v1/map/all?langcode=' + lang;
    var currentExist = false;
    var latitude = $('#alain-map').attr('latitude');
    var longitude = $('#alain-map').attr('longitude');

    $.getJSON(path, function (data) {
      data.features.forEach(function (marker) {
        // create a DOM element for the marker
        var icon = document.createElement('div');
        icon.className = 'marker ' + marker.properties.id;
        icon.style.backgroundImage = 'url(' + marker.properties.icon + ')';
        icon.style.width = 50 + 'px';
        icon.style.height = 50 + 'px';


        var language = '';
        if (lang == 'ar') {
          language = '/ar';
        }

        var explore = '<div class="map-more-wrapper"><a class="map-more" href="' + language + '/node/' + marker.properties.id + '">' + Drupal.t('Explore') + '</a></div>'
        var nodeUrl = '<h2><a href="' + marker.properties.path + '">' + marker.properties.name + '</a></h2>'
       
        if (marker.properties.type == 'venue') {
          explore = '';
          nodeUrl = '<h2>' + marker.properties.name + '</h2>';
        }

        var popup = new mapboxgl.Popup()
          .setHTML(
            '<div class="map-popup">' +
            '<div class="col-sm-8">' +
            nodeUrl +
            '<div class="map-details">' + marker.properties.body + '</div>' +
            explore +
            '</div>' +
            '<div class="col-sm-4"><img src="' + marker.properties.thumbnail + '"/></div>' +
            '</div>');

        //search value

        searchArray[count] = {value: marker.properties.id, label: marker.properties.name};
        count++;

        $("#edit-animals--wrapper input.form-checkbox").change(function () {
          if (this.checked) {
            if ($(this).val() == marker.properties.category) {
              icon.style.width = 50 + 'px';
              icon.style.height = 50 + 'px';
            }
          }
          else {
            if ($(this).val() == marker.properties.category) {
              icon.style.width = 0 + 'px';
              icon.style.height = 0 + 'px';
            }
          }
        });

        // Add marker to map
        var current = new mapboxgl.Marker(icon)
          .setLngLat(marker.geometry.coordinates)
          .setPopup(popup)
          .addTo(map);

        icon.addEventListener('click', function () {
          map.flyTo({center: marker.geometry.coordinates, zoom: 17.5, speed: 0.4, curve: 1});
          if ($(this).hasClass('clicked')) {
            current.togglePopup();
            $(this).removeClass('clicked')
          }
        });

        if (longitude == marker.geometry.coordinates[0] && latitude == marker.geometry.coordinates[1]) {
          currentExist = true;
          map.flyTo({center: marker.geometry.coordinates, zoom: 17.5, speed: 0.4, curve: 1});
          current.togglePopup();
        }

        $('#edit-experience--wrapper input.form-checkbox, #edit-venue--wrapper input.form-checkbox').change(function () {
          if (popup.isOpen()) {
            current.togglePopup();
          }
          if (this.checked) {
            $('#edit-experience--wrapper input.form-checkbox, #edit-venue--wrapper input.form-checkbox').prop("checked", false)
            $(this).prop("checked", true)
            if (($(this).val() == marker.properties.id)) {
              map.flyTo({center: marker.geometry.coordinates, zoom: 17.5, speed: 0.4, curve: 1});
              current.togglePopup();
            }
          }
        });

        $("#edit-animals--wrapper input.form-checkbox").change(function () {
          if (popup.isOpen()) {
            current.togglePopup();
          }
        })
      });

      if(!currentExist) {
        if(longitude && latitude) {
          if(longitude != 'all'){
            var requestIcon = document.createElement('div');
            requestIcon.className = 'marker request';
            requestIcon.style.backgroundImage = 'url(/themes/ain/images/map-marker-icon.png)';
            requestIcon.style.width = 50 + 'px';
            requestIcon.style.height = 50 + 'px';

            var message = new mapboxgl.Popup()
              .setHTML('<h1>' + Drupal.t('Request HERE') + '</h1>');

            var request = new mapboxgl.Marker(requestIcon)
              .setLngLat([longitude, latitude])
              .setPopup(message)
              .addTo(map);
            map.flyTo({ center: [longitude, latitude], zoom: 17.5, speed: 0.4, curve: 1 });
            request.togglePopup();
          }
        }
      }    
    });

    $('#edit-search').autocomplete({
      source: searchArray,
      select: function (event, ui) {
        event.preventDefault();
        $('#edit-search').val(ui.item.label);
        $('.marker.' + ui.item.value).addClass('clicked').click();
      },
      minLength: 2
    })
  });

}
)(window.jQuery, window._, window.Drupal, window.drupalSettings, window.localStorage);
