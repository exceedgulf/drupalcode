<?php

/**
 * @file
 * Displays children pages as a block
 */

namespace Drupal\ain_misc\Plugin\Block;

use Drupal\system\Plugin\Block\SystemMenuBlock;

/**
 * Provides a 'Secondary menu block' block.
 *
 * Drupal\Core\Block\BlockBase gives us a very useful set of basic functionality
 * for this configurable block. We can just fill in a few of the blanks with
 * defaultConfiguration(), blockForm(), blockSubmit(), and build().
 *
 * @Block(
 *   id = "ain_menu_block",
 *   admin_label = @Translation("Ain Menu Block"),
 *   category = @Translation("Menu")
 * )
 */
class AinMenuBlock extends SystemMenuBlock {

  public function build() {
    $build['#cache']['max-age'] = 0;

    $menu_name = 'main';

    $parameters = $this->menuTree->getCurrentRouteMenuTreeParameters($menu_name);
    $level = 2;
    $depth = 1;
    $expand = 0;
    $parent = $menu_name . ':';

    $parameters->setMinDepth($level);
    if ($depth > 0) {
      $parameters->setMaxDepth(min($level + $depth - 1, $this->menuTree->maxDepth()));
    }
    if ($expand) {
      $parameters->expandedParents = array();
    }
    if ($menuLinkID = str_replace($menu_name . ':', '', $parent)) {
      $parameters->setRoot($menuLinkID);
    }

    if (!isset($tree)) {
      $tree = $this->menuTree->load($menu_name, $parameters);
    }

    $manipulators = array(
      array('callable' => 'menu.default_tree_manipulators:checkAccess'),
      array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort'),
    );
    $tree = $this->menuTree->transform($tree, $manipulators);
    $build = $this->menuTree->build($tree);

    if (!empty($build['#theme'])) {
      $build['#theme'] = 'menu';
    }

    $menu = array();
    $item = array();

    if (empty($build['#items'])) {
      return $menu;
    }
    $node = \Drupal::routeMatch()->getParameter('node');
    $nid = $node->id();
    foreach ($build['#items'] as $menu_link) {
      if ($menu_link['url']->getRouteParameters()['node'] !== $nid) {
        $item['text'] = $menu_link['title'];
        $item['url'] = $menu_link['url']->toString();
        $menu[] = $item;
      }
    }

    return $menu;
  }

}
