<?php

/**
 * @file
 * Definition of Drupal\ain_misc\Plugin\views\field\NodeTypeFlagger
 */

namespace Drupal\ain_misc\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to flag the node type.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("user_custom_email")
 */
class UserCustomEmail extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    $route_match = \Drupal::routeMatch();
    if (in_array($route_match->getRouteName(), array('entity.user.canonical', 'entity.user.edit_form'))) {
      $user = \Drupal::routeMatch()->getParameter('user');
      if ($user->get('mail') && $user->get('mail')->getValue()) {
        $email = $user->get('mail')->getValue()[0]['value'];
        return $email;
      } else {
        return '';
      }
    }
    else {
      return '';
    }
  }

}
