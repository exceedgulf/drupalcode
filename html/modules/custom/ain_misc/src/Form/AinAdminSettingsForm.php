<?php

namespace Drupal\ain_misc\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure AinAdminSettingsForm settings for this site.
 */
class AinAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ain_misc_form_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ain_misc.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('ain_misc.settings');

    $form['tabs_wrapper'] = array(
      '#type' => 'vertical_tabs'
    );

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('Static Nodes'),
      '#markup' => '<p>Configure static node ids that will be used in the code.',
      '#group' => 'tabs_wrapper'
    ];

    $form['general']['multilingual'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Multilingual Content'),
      '#open' => TRUE
    ];

    $form['general']['multilingual']['ain_misc_plan_visit'] = [
      '#type' => 'number',
      '#title' => $this->t('Plan Your Visit'),
      '#step' => 1,
      '#description' => $this->t('Node id for <em>Plan your visit</em> page.'),
      '#required' => TRUE,
      '#default_value' => ($config->get('ain_misc_plan_visit')) ? $config->get('ain_misc_plan_visit') : 391,
    ];

    $form['general']['multilingual']['ain_misc_do_do_not'] = [
      '#type' => 'number',
      '#title' => $this->t('Do and Don\'t'),
      '#step' => 1,
      '#description' => $this->t('Node id for <em>Do and Don\'t</em> page.'),
      '#required' => TRUE,
      '#default_value' => ($config->get('ain_misc_do_do_not')) ? $config->get('ain_misc_do_do_not') : 1256,
    ];

    $form['general']['multilingual']['ain_misc_attraction_szdlc'] = [
      '#type' => 'number',
      '#title' => $this->t('Attraction: SZDLC'),
      '#step' => 1,
      '#description' => $this->t('Node id for <em>Attraction: SZDLC</em> page.'),
      '#required' => TRUE,
      '#default_value' => ($config->get('ain_misc_attraction_szdlc')) ? $config->get('ain_misc_attraction_szdlc') : 174,
    ];

    $form['general']['multilingual']['ain_misc_attraction_safari'] = [
      '#type' => 'number',
      '#title' => $this->t('Attraction: Safari'),
      '#step' => 1,
      '#description' => $this->t('Node id for <em>Attraction: Safari</em> page.'),
      '#required' => TRUE,
      '#default_value' => ($config->get('ain_misc_attraction_safari')) ? $config->get('ain_misc_attraction_safari') : 175,
    ];

    $form['general']['non_multilingual'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Non Multilingual Content'),
      '#open' => TRUE
    ];

    $form['general']['non_multilingual']['ain_misc_ticket_safari_explorer'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Ticket: Safari Explorer'),
      '#open' => TRUE,
      '#attributes' => [
        'class' => ['non-multilingual-wrapper'],
      ],
    ];

    $form['general']['non_multilingual']['ain_misc_ticket_safari_explorer']['ain_misc_ticket_safari_explorer_en'] = [
      '#type' => 'number',
      '#title' => $this->t('English'),
      '#step' => 1,
      '#description' => $this->t('Node id for <em>Ticket: Safari Explorer (EN)</em>.'),
      '#required' => TRUE,
      '#default_value' => ($config->get('ain_misc_ticket_safari_explorer_en')) ? $config->get('ain_misc_ticket_safari_explorer_en') : 2492,
    ];

    $form['general']['non_multilingual']['ain_misc_ticket_safari_explorer']['ain_misc_ticket_safari_explorer_ar'] = [
      '#type' => 'number',
      '#title' => $this->t('Arabic'),
      '#step' => 1,
      '#description' => $this->t('Node id for <em>Ticket: Safari Explorer (AR)</em>.'),
      '#required' => TRUE,
      '#default_value' => ($config->get('ain_misc_ticket_safari_explorer_ar')) ? $config->get('ain_misc_ticket_safari_explorer_ar') : 2491,
    ];

    $form['general']['non_multilingual']['ain_misc_ticket_explorer_ticket'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Ticket: Explorer Ticket'),
      '#open' => TRUE,
      '#attributes' => [
        'class' => ['non-multilingual-wrapper'],
      ],
    ];

    $form['general']['non_multilingual']['ain_misc_ticket_explorer_ticket']['ain_misc_ticket_explorer_ticket_en'] = [
      '#type' => 'number',
      '#title' => $this->t('English'),
      '#step' => 1,
      '#description' => $this->t('Node id for <em>Ticket: Explorer Ticket (EN)</em>.'),
      '#required' => TRUE,
      '#default_value' => ($config->get('ain_misc_ticket_explorer_ticket_en')) ? $config->get('ain_misc_ticket_explorer_ticket_en') : 2490,
    ];

    $form['general']['non_multilingual']['ain_misc_ticket_explorer_ticket']['ain_misc_ticket_explorer_ticket_ar'] = [
      '#type' => 'number',
      '#title' => $this->t('Arabic'),
      '#step' => 1,
      '#description' => $this->t('Node id for <em>Ticket: Explorer Ticket (AR)</em>.'),
      '#required' => TRUE,
      '#default_value' => ($config->get('ain_misc_ticket_explorer_ticket_ar')) ? $config->get('ain_misc_ticket_explorer_ticket_ar') : 2489,
    ];

    $form['general']['non_multilingual']['ain_misc_ticket_safari_entertainer'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Ticket: Safari Entertainer'),
      '#open' => TRUE,
      '#attributes' => [
        'class' => ['non-multilingual-wrapper'],
      ],
    ];

    $form['general']['non_multilingual']['ain_misc_ticket_safari_entertainer']['ain_misc_ticket_safari_entertainer_en'] = [
      '#type' => 'number',
      '#title' => $this->t('English'),
      '#step' => 1,
      '#description' => $this->t('Node id for <em>Ticket: Safari Entertainer (EN)</em>.'),
      '#required' => TRUE,
      '#default_value' => ($config->get('ain_misc_ticket_safari_entertainer_en')) ? $config->get('ain_misc_ticket_safari_entertainer_en') : 1355,
    ];

    $form['general']['non_multilingual']['ain_misc_ticket_safari_entertainer']['ain_misc_ticket_safari_entertainer_ar'] = [
      '#type' => 'number',
      '#title' => $this->t('Arabic'),
      '#step' => 1,
      '#description' => $this->t('Node id for <em>Ticket: Safari Entertainer (AR)</em>.'),
      '#required' => TRUE,
      '#default_value' => ($config->get('ain_misc_ticket_safari_entertainer_ar')) ? $config->get('ain_misc_ticket_safari_entertainer_ar') : 1562,
    ];

    $form['general']['non_multilingual']['ain_misc_ticket_entertainer_ticket'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Ticket: Entertainer Ticket'),
      '#open' => TRUE,
      '#attributes' => [
        'class' => ['non-multilingual-wrapper'],
      ],
    ];

    $form['general']['non_multilingual']['ain_misc_ticket_entertainer_ticket']['ain_misc_ticket_entertainer_ticket_en'] = [
      '#type' => 'number',
      '#title' => $this->t('English'),
      '#step' => 1,
      '#description' => $this->t('Node id for <em>Ticket: Entertainer Ticket (EN)</em>.'),
      '#required' => TRUE,
      '#default_value' => ($config->get('ain_misc_ticket_entertainer_ticket_en')) ? $config->get('ain_misc_ticket_entertainer_ticket_en') : 1278,
    ];

    $form['general']['non_multilingual']['ain_misc_ticket_entertainer_ticket']['ain_misc_ticket_entertainer_ticket_ar'] = [
      '#type' => 'number',
      '#title' => $this->t('Arabic'),
      '#step' => 1,
      '#description' => $this->t('Node id for <em>Ticket: Entertainer Ticket (AR)</em>.'),
      '#required' => TRUE,
      '#default_value' => ($config->get('ain_misc_ticket_entertainer_ticket_ar')) ? $config->get('ain_misc_ticket_entertainer_ticket_ar') : 2217,
    ];

    $form['request'] = [
      '#type' => 'details',
      '#title' => $this->t('Request: Notification Email'),
      '#group' => 'tabs_wrapper'
    ];

    $form['request']['ain_request_notification_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email Address'),
      '#placeholder' => $this->t('Email Address'),
      '#description' => $this->t('Email address to receive notifications after new <em>Request</em> is submitted, examples: new <em>SOS</em> or <em>Shuttle</em> request.'),
      '#required' => TRUE,
      '#default_value' => ($config->get('ain_request_notification_email')) ? $config->get('ain_request_notification_email') : \Drupal::config('system.site')->get('mail'),
    ];

    $form['sms'] = [
      '#type' => 'details',
      '#title' => $this->t('SMS Configurations'),
      '#group' => 'tabs_wrapper'
    ];

    $form['sms']['ain_sms_mobile_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mobile Number'),
      '#placeholder' => $this->t('Mobile Number'),
      '#description' => $this->t('Mobile number to receive SMS notifications.'),
      '#required' => TRUE,
      '#default_value' => ($config->get('ain_sms_mobile_number')) ? $config->get('ain_sms_mobile_number') : '0562189666',
    ];

    $form['sms']['ain_sms_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#placeholder' => $this->t('Username'),
      '#description' => $this->t('SMS service username.'),
      '#required' => TRUE,
      '#default_value' => ($config->get('ain_sms_username')) ? $config->get('ain_sms_username') : 'sosuser',
    ];

    $form['sms']['ain_sms_password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#placeholder' => $this->t('Password'),
      '#description' => $this->t('SMS service password. <br /<em><b>Note:</b> Leave blank if you don\'t want to change existing password.</em>'),
      '#default_value' => ($config->get('ain_sms_password')) ? $config->get('ain_sms_password') : 'usersos',
    ];

    $form['#attached']['library'][] = 'ain_misc/config_form';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ain_misc.settings');
    $config
      ->set('ain_misc_plan_visit', $form_state->getValue('ain_misc_plan_visit'))
      ->set('ain_misc_do_do_not', $form_state->getValue('ain_misc_do_do_not'))
      ->set('ain_misc_attraction_szdlc', $form_state->getValue('ain_misc_attraction_szdlc'))
      ->set('ain_misc_attraction_safari', $form_state->getValue('ain_misc_attraction_safari'))
      ->set('ain_misc_ticket_safari_explorer_en', $form_state->getValue('ain_misc_ticket_safari_explorer_en'))
      ->set('ain_misc_ticket_safari_explorer_ar', $form_state->getValue('ain_misc_ticket_safari_explorer_ar'))
      ->set('ain_misc_ticket_explorer_ticket_en', $form_state->getValue('ain_misc_ticket_explorer_ticket_en'))
      ->set('ain_misc_ticket_explorer_ticket_ar', $form_state->getValue('ain_misc_ticket_explorer_ticket_ar'))
      ->set('ain_misc_ticket_safari_entertainer_en', $form_state->getValue('ain_misc_ticket_safari_entertainer_en'))
      ->set('ain_misc_ticket_safari_entertainer_ar', $form_state->getValue('ain_misc_ticket_safari_entertainer_ar'))
      ->set('ain_misc_ticket_entertainer_ticket_en', $form_state->getValue('ain_misc_ticket_entertainer_ticket_en'))
      ->set('ain_misc_ticket_entertainer_ticket_ar', $form_state->getValue('ain_misc_ticket_entertainer_ticket_ar'))
      ->set('ain_request_notification_email', $form_state->getValue('ain_request_notification_email'))
      ->set('ain_sms_mobile_number', $form_state->getValue('ain_sms_mobile_number'))
      ->set('ain_sms_username', $form_state->getValue('ain_sms_username'));

    if ($form_state->getValue('ain_sms_password') != '') {
      $config->set('ain_sms_password', $form_state->getValue('ain_sms_password'));
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
