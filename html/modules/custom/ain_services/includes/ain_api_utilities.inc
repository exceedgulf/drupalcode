<?php

/**
 * @file
 * Strings translation for Rest resources messages.
 */

/**
 * Helper function to translate English into Arabic.
 *
 * @param string|object $source
 *   Source English value.
 *
 * @return string $translation
 *   The Arabic translation of the provided English string.
 */
function __translate_to_ar($source)
{

  $tranalation_map = [
    'An illegal choice has been detected. Please contact the site administrator.' =>'تم التعرف على اختيار غير مسموح به. من فضلك اتصل بإداري الموقع.',
    'Failed: Invalid webform_id value.' =>'فشل: لم يتم العثور على النموذج المطلوب.',
    'Name field is required.' => 'حقل الإسم ضروري.',
    'Service Name field is required.' => 'حقل إسم الخدمة ضروري.',
    'Phone Number field is required.' => 'حقل رقم الهاتف ضروري.',
    'Service Date field is required.' => 'حقل التاريخ ضروري.',
    '<em class="placeholder">Service Time</em> must be a valid time.' => '<em class="placeholder">وقت الخدمة</em> يجب أن يكون وقتا صحيحا.',
    'Unrecognized username or email address.' => 'لم يتم العثور على إسم المستخدم أو البريد الإلكتروني.',
    'Password reset instructions is sent to' => 'تم إرسال تعليمات إعادة تعيين كلمة المرور إلى ',
    'Unable to send email. Contact the site administrator if the problem persists.' => 'تعذر إرسال البريد الإلكتروني. من فضلك اتصل بإداري الموقع.',
    'The user has not been activated or is blocked.' => 'حساب المستخدم لم يتم تفعيله أو تم حظر المستخدم.',
    'Missing username or email address.' => 'الرجاء إدخال إسم المستخدم أو البريد الإلكتروني.',
    'Missing password.' => 'الرجاء إدخال كلمة المرور.',
    'Missing username.' => 'الرجاء إدخال إسم المستخدم.',
    'Unrecognized email address.' => 'لم يتم العثور على البريد الإلكتروني.',
    'Unrecognized user or password.' => 'لم يتم العثور على المستخدم أو كلمة السر خاطئة.',
    'You have been successfully logged in.' => 'تم تسجيل الدخول بنجاح.',
    'You have been successfully logged out.' => 'تم تسجيل الخروج بنجاح.',
    'Missing mobile number or email address.' => 'رقم الهاتف أو البريد الإلكتروني مفقود.',
    'Unrecognized mobile number or email address.' => 'لم يتم العثور على رقم الهاتف أو البريد الإلكتروني.',
    'Unrecognized mobile number, email address or password.' => 'لم يتم العثور على رقم الهاتف أو البريد الإلكتروني أو كلمة المرور.',
    'No user account data for registration received.' => 'لم يتم استلام بيانات حساب المستخدم من اجل للتسجيل.',
    'An ID has been set and only new user accounts can be registered.' => 'تم تعيين هوية مستخدم ويمكن تسجيل حسابات جديدة فقط.',
    'You cannot register a new user account.' => 'لا يمكنك تسجيل حساب مستخدم جديد.',
    'No password provided.' => 'لم يتم توفير كلمة مرور.',
    'A Password cannot be specified. It will be generated on login.' => 'لا يمكن تحديد كلمة المرور. سيتم إنشاء واحدة عند تسجيل الدخول.',
    'Invalid mobile number.' => 'رقم الهاتف غير صالح.',
    'Date of birth should be in the past.' => 'تاريخ الميلاد ينبغي أن يكون في الماضي.',
    'Invalid Name, name should not include numbers.' => 'اسم غير صالح، لا ينبغي أن يشمل الاسم على أرقام.',
    'User account is created successfully, Starting points have credited to your account!' => 'تم إنشاء حساب المستخدم بنجاح ، لقد تم إضافة نقاط البدء إلى الحساب!',
    'Success' => 'نجح',
    'Failed' => 'فشل',
    'Family member is created successfully' => 'تمت إضافة عضو جديد بنجاح',
    'You are allowed to create only one visit plan.' => 'يوجد لديك خطة فعالة، الرجاء حذفها.',
    'visit plan was deleted successfully' => 'تم حذف الخطة بنجاح.',
    'Your family member was removed successfully.' => 'تمت إزالة عضو عائلتك بنجاح.',
    'You’ve used this Clue already, please look for new clues' => 'لقد استخدمت هذا الدليل، يرجى البحث عن دليل جديد'
  ];

  switch (getType($source)) {
    case 'object':
      if (get_class($source) == 'Drupal\\Core\\StringTranslation\\TranslatableMarkup') {
        $source = $source->render();
      }
      break;
  }

  $translation = $tranalation_map[$source];

  return (!is_null($translation)) ? $translation : $source;
}
