<?php

namespace Drupal\ain_services\Normalizer;

use Drupal\serialization\Normalizer\NormalizerBase;
use Drupal\Component\Serialization\Json;
use Drupal\views\Views;

/**
 * Converts typed data objects to arrays.
 */
class CustomTypedDataNormalizer extends NormalizerBase {

  /**
   * The interface or class that this Normalizer supports.
   *
   * @var string
   */
  protected $supportedInterfaceOrClass = 'Drupal\Core\TypedData\TypedDataInterface';

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = array()) {
    $value = $object->getValue();
    if (isset($value[0]) && isset($value[0]['value'])) {
      $value = $value[0]['value'];
    }

    if ($object->getName() == 'field_family_member') {
      $value = $object->getValue();
      foreach ($value as &$val) {
        $fid = $val['target_id'];
        $args = [$fid];
        $view = Views::getView('api_services');
        $view->setArguments($args);
        $view->setDisplay('family_member');
        $view->preExecute();
        $view->execute();
        $content = $view->render();
        $data_string = $content['#markup']->jsonSerialize();
        $val = Json::decode($data_string);
      }
    }

    return $value;
  }

}
