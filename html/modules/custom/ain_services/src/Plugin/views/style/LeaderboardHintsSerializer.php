<?php

/**
 * @file
 * Contains \Drupal\ain_services\Plugin\views\style\LeaderboardHintsSerializer.
 */

namespace Drupal\ain_services\Plugin\views\style;

use Drupal\rest\Plugin\views\style\Serializer;
use Drupal\Component\Serialization\Json;
use Drupal\views\Views;
use Drupal\Component\Render\MarkupTrait;
use Drupal\Core\Url;

/**
 * The style plugin for serialized output formats.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "leaderboard_hints_serializer",
 *   title = @Translation("TH Not Completed Serializer"),
 *   help = @Translation("Serializes views row data using the LeaderboardHintsSerializer component."),
 *   display_types = {"data"}
 * )
 */
class LeaderboardHintsSerializer extends Serializer {

  /**
   * {@inheritdoc}
   */
  public function render() {

    $rows = [];
    $user_id = \Drupal::currentUser()->id();
    $rank = FALSE;
    $current_url = Url::fromRoute('<current>');
    $path = $current_url->getInternalPath();
    $path_args = explode('/', $path);
    $treasure_hunt_id = $path_args[5];

    foreach ($this->view->result as $row_index => $row) {
      $rendered_row = $this->view->rowPlugin->render($row);
      if(!$rank) {
        $completed = $this->getCompletedNumber($treasure_hunt_id);
        $page = (isset($_GET['page'])) ? $_GET['page'] : 0;
        $rank = $completed + 1 + (($page) * 10);
      }
      unset($rendered_row['treasure_hunt_id']);

      // Add current user.
      if ($user_id == $rendered_row['user_id']) {
        $rendered_row = ['current_user' => true] + $rendered_row;
      }
      else {
        $rendered_row = ['current_user' => false] + $rendered_row;
      }

      // Add Rank.
      $rendered_row = ['rank' => $rank] + $rendered_row;
      $rank++;

      $rows[] = $rendered_row;
    }

    $result = [
      'result' => $rows
    ];

    return $this->serializer->serialize($result, 'json');
  }

  function getCompletedNumber($treasure_hunt_id) {
    $start = strtotime('today 00:00:00');
    $end = strtotime('today 23:59:59');

    $query = \Drupal::database()->select('gamification_field_data', 'points');
    $query->fields('points', ['created']);
    $query->condition('points.field_type', 'treasure_hunt');
    $query->condition('points.created', $start, '>=');
    $query->condition('points.created', $end, '<=');
    $query->condition('points.field_treasure_hunt', $treasure_hunt_id);
    return $query->countQuery()->execute()->fetchField(); 
  }

}
