<?php

/**
 * @file
 * Contains \Drupal\ain_services\Plugin\views\style\LeaderboardSerializer.
 */

namespace Drupal\ain_services\Plugin\views\style;

use Drupal\rest\Plugin\views\style\Serializer;
use Drupal\Component\Serialization\Json;
use Drupal\views\Views;
use Drupal\Core\Render\Markup;

/**
 * The style plugin for serialized output formats.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "leaderboard_serializer",
 *   title = @Translation("TH Completed Serializer"),
 *   help = @Translation("Serializes views row data using the LeaderboardSerializer component."),
 *   display_types = {"data"}
 * )
 */
class LeaderboardSerializer extends Serializer {

  /**
   * {@inheritdoc}
   */
  public function render() {
    $rows = [];

    $page = (isset($_GET['page'])) ? $_GET['page'] : 0;
    $rank = 1 + (($page) * 10);

    $user_id = \Drupal::currentUser()->id();
    $treasure_hunt_id = $this->view->args[0];
    $details = false;

    foreach ($this->view->result as $row_index => $row) {
      $rendered_row = $this->view->rowPlugin->render($row);
      if(!$details) {
        $start = 0;
        $end = 0;
        $details = $this->getCompletedDetails($treasure_hunt_id);
      }
      unset($rendered_row['treasure_hunt_id']);
      
      // Add current user.
      if ($user_id == $rendered_row['user_id']) {
        $rendered_row = ['current_user' => true] + $rendered_row;
      }
      else {
        $rendered_row = ['current_user' => false] + $rendered_row;
      }

      // Add rank.
      $rendered_row = ['rank' => $rank] + $rendered_row;
      $rank++;

      // Add hints and points.
      $detail = $details[$rendered_row['user_id']->__toString()];
      $rendered_row = $rendered_row + [
        'hints' => $detail['hints'],
        'points' => $detail['points'],
      ];
      $rows[] = $rendered_row;
    }

    $result = [
      'result' => $rows
    ];

    // Add current user rank for the first page only.
    if ($page == 0) {
      $current_user_rank = 'n/a';
      $complete = $this->userCompleteTreasureHunt($user_id, $treasure_hunt_id);

      if (!$complete) {
        $has_hints = $this->userHasCompletedHints($user_id, $treasure_hunt_id);

        if ($has_hints) {
          $current_user_rank = $this->getUserHintsRank($user_id, $treasure_hunt_id);
        }
      }
      else {
        $current_user_rank = $this->getUserCompletedRank($complete, $treasure_hunt_id) + 1;
      }

      $result = $result + [
        'current_user' => [
          ['rank' => $current_user_rank] + $this->getUserDetails($treasure_hunt_id, $user_id)
        ]
      ];
    }

    return $this->serializer->serialize($result, 'json');
  }

  public function userCompleteTreasureHunt($user_id, $treasure_hunt_id) {
    $start = strtotime('today 00:00:00');
    $end = strtotime('today 23:59:59');

    $query = \Drupal::database()->select('gamification_field_data', 'points');
    $query->fields('points', ['created']);
    $query->condition('points.field_type', 'treasure_hunt');
    $query->condition('points.field_cuid', $user_id);
    $query->condition('points.created', $start, '>=');
    $query->condition('points.created', $end, '<=');
    $query->condition('points.field_treasure_hunt', $treasure_hunt_id);
    return $query->execute()->fetchField();
  }

  public function userHasCompletedHints($user_id, $treasure_hunt_id) {
    $start = strtotime('today 00:00:00');
    $end = strtotime('today 23:59:59');

    $query = \Drupal::database()->select('gamification_field_data', 'points');
    $query->fields('points', ['created']);
    $query->condition('points.field_type', 'hint');
    $query->condition('points.field_cuid', $user_id);
    $query->condition('points.created', $start, '>=');
    $query->condition('points.created', $end, '<=');
    $query->condition('points.field_treasure_hunt', $treasure_hunt_id);

    return $query->execute()->fetchField();
  }

  public function getCompletedNumber($treasure_hunt_id) {
    $start = strtotime('today 00:00:00');
    $end = strtotime('today 23:59:59');

    $query = \Drupal::database()->select('gamification_field_data', 'points');
    $query->fields('points', ['created']);
    $query->condition('points.field_type', 'treasure_hunt');
    $query->condition('points.created', $start, '>=');
    $query->condition('points.created', $end, '<=');
    $query->condition('points.field_treasure_hunt', $treasure_hunt_id);
    return $query->countQuery()->execute()->fetchField();
  }

  public function getUserHintsRank($user_id,  $treasure_hunt_id) {
    $start = strtotime('today 00:00:00');
    $end = strtotime('today 23:59:59');
    $query = db_query("SELECT gamification.field_cuid, COUNT(gamification.field_hint), SUM(gamification.field_point)
                                            FROM gamification_field_data gamification
                                            WHERE 
                                                gamification.field_treasure_hunt = " . $treasure_hunt_id . "        
                                              AND
                                                gamification.field_cuid NOT IN (
                                                  SELECT completed.field_cuid 
                                                  FROM gamification_field_data completed
                                                  WHERE completed.field_type = 'treasure_hunt'
                                                  AND completed.field_treasure_hunt = " . $treasure_hunt_id . "
                                                  AND completed.created >= '" . $start . "'
                                                  AND completed.created <= '" . $end . "'
                                                )
                                              AND
                                                  gamification.field_type = 'hint'
                                              AND 
                                                  gamification.created >= '" . $start . "'
                                              AND 
                                                  gamification.created <= '" . $end . "'
                                            GROUP BY  gamification.field_cuid
                                            ORDER BY COUNT(gamification.field_hint) DESC, SUM(gamification.field_point) DESC");
    $hints_users = $query->fetchAll();
    foreach ($hints_users as $user_rank => $user) {
      if ($user->field_cuid == $user_id) {
        return $user_rank + 1 + $this->getCompletedNumber($treasure_hunt_id);
      }
    }
  }

  public function getCompletedDetails($treasure_hunt_id) {
    $start = strtotime('today 00:00:00');
    $end = strtotime('today 23:59:59');
    $query = db_query("SELECT gamification.field_cuid AS user_id, COUNT(gamification.field_hint) AS hints, SUM(gamification.field_point) AS points
                       FROM gamification_field_data gamification
                       WHERE 
                           gamification.field_treasure_hunt = " . $treasure_hunt_id . "        
                         AND
                           gamification.field_cuid IN (
                             SELECT completed.field_cuid 
                             FROM gamification_field_data completed
                             WHERE completed.field_type = 'treasure_hunt'
                             AND completed.field_treasure_hunt = " . $treasure_hunt_id  . "
                             AND completed.created >= '" . $start . "'
                             AND completed.created <= '" . $end . "'
                           )
                         AND
                             (gamification.field_type = 'hint' or gamification.field_type = 'treasure_hunt')
                         AND 
                             gamification.created >= '" . $start . "'
                         AND 
                             gamification.created <= '" . $end . "'
                       GROUP BY gamification.field_cuid
                       ORDER BY COUNT(gamification.field_hint) DESC, SUM(gamification.field_point) DESC");
    $results = $query->fetchAll();
    
    $details = [];
    foreach ($results as $key => $result) {
      $details[$result->user_id] = [
        'hints' => $result->hints,
        'points' => $result->points,
      ];
    }
    return $details;
    
  }

  public function getUserDetails($treasure_hunt_id, $user_id){
    $start = strtotime('today 00:00:00');
    $end = strtotime('today 23:59:59');
    $query = db_query("SELECT gamification.field_cuid AS user_id, COUNT(gamification.field_hint) AS hints, SUM(gamification.field_point) AS points
                       FROM gamification_field_data gamification
                       WHERE 
                           gamification.field_treasure_hunt = " . $treasure_hunt_id . "        
                         AND
                           gamification.field_cuid = " . $user_id . "
                         AND
                             (gamification.field_type = 'hint' or gamification.field_type = 'treasure_hunt')
                         AND 
                             (gamification.created >= '" . $start . "')
                         AND 
                             (gamification.created <= '" . $end . "')
                       GROUP BY  gamification.field_cuid");
    $results = $query->fetchObject();
    return [
      'hints' => $results->hints,
      'points' => $results->points,
    ];
  }

  public function getUserCompletedRank($complete, $treasure_hunt_id){
    $start = strtotime('today 00:00:00');
    $end = $complete;

    $query = \Drupal::database()->select('gamification_field_data', 'points');
    $query->fields('points', ['created']);
    $query->condition('points.field_type', 'treasure_hunt');
    $query->condition('points.field_treasure_hunt', $treasure_hunt_id);
    $query->condition('points.created', $start, '>=');
    $query->condition('points.created', $end, '<');
    return $query->countQuery()->execute()->fetchField();
    
  }
}