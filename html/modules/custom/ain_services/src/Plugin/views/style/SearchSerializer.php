<?php

/**
 * @file
 * Contains \Drupal\ain_services\Plugin\views\style\SearchSerializer.
 */

namespace Drupal\ain_services\Plugin\views\style;

use Drupal\rest\Plugin\views\style\Serializer;
use Drupal\Core\Datetime\DateHelper;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Component\Serialization\Json;

/**
 * The style plugin for serialized output formats.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "search_serializer",
 *   title = @Translation("Search Serializer"),
 *   help = @Translation("Serializes views row data using the SearchSerializer component."),
 *   display_types = {"data"}
 * )
 */
class SearchSerializer extends Serializer {

  /**
   * {@inheritdoc}
   */
  public function render() {
    $langcode = \Drupal::request()->query->get('langcode');
    if (!isset($langcode)) {
      $langcode = 'en';
    }
    $rows = [];

    foreach ($this->view->result as $row_index => $row) {
      $rendered_row = $this->view->rowPlugin->render($row);

      $type = $rendered_row['type'];
      switch ($type) {
        case 'events':
          $event_dates = Json::decode($rendered_row['date']);
          unset($rendered_row['date']);
          unset($rendered_row['langcode']);

          $time_value = [];
          foreach ($event_dates as $event_date) {
            $start_date_time = new \DateTime($event_date['start_date'], new \DateTimeZone('GMT'));
            $start_date_time->setTimezone(new \DateTimeZone(drupal_get_user_timezone()));
            $end_date_time = new \DateTime($event_date['end_date'], new \DateTimeZone('GMT'));
            $end_date_time->setTimezone(new \DateTimeZone(drupal_get_user_timezone()));

            $start_day = $start_date_time->format('Y-m-d');
            $start_time = $start_date_time->format('H:i');
            $end_day = $end_date_time->format('Y-m-d');
            $end_time = $end_date_time->format('H:i');

            $current_date = \Drupal::time()->getCurrentTime();
            $current_time = DrupalDateTime::createFromTimestamp($current_date);


            $start_day_raw = DrupalDateTime::createFromTimestamp(strtotime($start_day), drupal_get_user_timezone());
            $start_time_raw = DrupalDateTime::createFromTimestamp(strtotime($start_time), drupal_get_user_timezone());
            $end_day_raw = DrupalDateTime::createFromTimestamp(strtotime($end_day), drupal_get_user_timezone());
            $end_time_raw = DrupalDateTime::createFromTimestamp(strtotime($end_time), drupal_get_user_timezone());
            if ($current_time >= $start_day_raw && $current_time <= $end_day_raw && $current_time >= $start_time_raw && $current_time <= $end_time_raw) {
              $time_value[] = [
                'start_time' => $start_time,
                'end_time' => $end_time
              ];
            }
          }

          if (count($time_value)) {
            $rows[] = $rendered_row;
          }

          break;

        default:
          unset($rendered_row['date']);
          unset($rendered_row['langcode']);

          $rows[] = $rendered_row;
          break;
      }
    }

    return $this->serializer->serialize($rows, 'json');
  }

}
