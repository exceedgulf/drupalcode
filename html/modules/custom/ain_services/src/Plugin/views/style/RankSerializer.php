<?php

/**
 * @file
 * Contains \Drupal\ain_services\Plugin\views\style\RankSerializer.
 */

namespace Drupal\ain_services\Plugin\views\style;

use Drupal\rest\Plugin\views\style\Serializer;
use Drupal\Component\Serialization\Json;
use Drupal\views\Views;

/**
 * The style plugin for serialized output formats.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "rank_serializer",
 *   title = @Translation("Rank Serializer"),
 *   help = @Translation("Serializes views row data using the RankSerializer component."),
 *   display_types = {"data"}
 * )
 */
class RankSerializer extends Serializer {

  /**
   * {@inheritdoc}
   */
  public function render() {
    $langcode = \Drupal::request()->query->get('langcode');
    if (!isset($langcode)) {
      $langcode = 'en';
    }
    $rows = [];
    $rank = 1;
    $user_found = false;
    $type = '';

    $user_id = \Drupal::currentUser()->id();

    foreach ($this->view->result as $row_index => $row) {
      $rendered_row = $this->view->rowPlugin->render($row);
      $type = $rendered_row['type'];
      unset($rendered_row['type']);

      $rendered_row['rank'] = $rank;
      $rank++;

      $user_string = $rendered_row['user'];
      $user_data = Json::decode($user_string);
      $resource_user_data = $user_data[0]['id'];

      if ($user_id == $resource_user_data) {
        $rendered_row = ['current_user' => true] + $rendered_row;
        $user_found = true;
      }
      else {
        $rendered_row = ['current_user' => false] + $rendered_row;
      }

      $rows[] = $rendered_row;
    }

    if (!$user_found) {
      switch ($type) {
        case 'today':
          $display_id = 'gamifications_leaderboard_today';
          break;

        case 'month':
          $display_id = 'gamifications_leaderboard_month';
          break;

        case 'last_month':
          $display_id = 'gamifications_leaderboard_last_month';
          break;

        default:
          break;
      }
      $args = [$user_id];
      $view = Views::getView('user_gamification');
      $view->setArguments($args);
      $view->setDisplay($display_id);
      $view->preExecute();
      $view->execute();
      $content = $view->render();
      $data_string = $content['#markup']->jsonSerialize();
      $data = Json::decode($data_string)[0];

      if (count($data)) {
        $data['rank'] = 'n/a';
        $data = ['current_user' => true] + $data;
        unset($data['type']);

        $rows[] = $data;
      }
    }

    return $this->serializer->serialize($rows, 'json');
  }

}
