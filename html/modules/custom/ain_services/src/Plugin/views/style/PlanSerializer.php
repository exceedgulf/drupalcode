<?php

/**
 * @file
 * Contains \Drupal\ain_map\Plugin\views\style\MapSerializer.
 */

namespace Drupal\ain_services\Plugin\views\style;

use Drupal\rest\Plugin\views\style\Serializer;

/**
 * The style plugin for serialized output formats.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "plan_serializer",
 *   title = @Translation("Plan Serializer"),
 *   help = @Translation("Serializes views row data using the PlanSerializer component."),
 *   display_types = {"data"}
 * )
 */
class PlanSerializer extends Serializer {

  /**
   * {@inheritdoc}
   */
  public function render() {
    $rows = array();

    foreach ($this->view->result as $row_index => $row) {
      $rendered_row = $this->view->rowPlugin->render($row);
      if(!empty($rendered_row['recommended_plan'])) {
        $rendered_row['suggested_tickets_en'] = [
          [
            'title' => $rendered_row['ticket_en'],
            'adult' => $rendered_row['adult'],
            'child' => $rendered_row['child'],
          ]
        ];
      }
      if (!empty($rendered_row['recommended_plan'])) {
        $rendered_row['suggested_tickets_ar'] = [
          [
            'title' => $rendered_row['ticket_ar'],
            'adult' => $rendered_row['adult'],
            'child' => $rendered_row['child'],
          ]
        ];
      }
      unset($rendered_row['ticket_en']);
      unset($rendered_row['ticket_ar']);
      unset($rendered_row['adult']);
      unset($rendered_row['child']);
      unset($rendered_row['recommended_plan']);
      $rows[] = $rendered_row;
      
    }
    
    return $this->serializer->serialize($rows, 'json');
  }

}
