<?php

/**
 * @file
 * Contains \Drupal\ain_services\Plugin\views\style\TreasureHuntSerializer.
 */

namespace Drupal\ain_services\Plugin\views\style;

use Drupal\rest\Plugin\views\style\Serializer;
use Drupal\Component\Serialization\Json;

/**
 * The style plugin for serialized output formats.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "treasure_hunt_serializer",
 *   title = @Translation("Treasure Hunt Serializer"),
 *   help = @Translation("Serializes views row data using the TreasureHuntSerializer component."),
 *   display_types = {"data"}
 * )
 */
class TreasureHuntSerializer extends Serializer {

  /**
   * {@inheritdoc}
   */
  public function render() {
    $rows = [];

    foreach ($this->view->result as $row_index => $row) {
      $rendered_row = $this->view->rowPlugin->render($row);

      $treasure_hunt_id = Json::decode($rendered_row['treasure_hunt']);
      $hints = Json::decode($rendered_row['hints']);
      $completed_hints = Json::decode($rendered_row['completed_hints']);
      unset($rendered_row['completed_hints']);
      $treasure_hunt_completed = TRUE;

      $order = 1;
      foreach ($hints as &$hint) {
        $hint = ['order' => $order] + $hint;
        $order++;
        if ($this->searchArray($completed_hints, $hint['id']) == true) {
          $hint['status'] = 'completed';
        }
        else {
          $treasure_hunt_completed = FALSE;
          $hint['status'] = 'pending';
        }
      }
      $rendered_row['treasure_hunt'] = [];
      $rendered_row['treasure_hunt']['id'] = $treasure_hunt_id;
      if ($treasure_hunt_completed) {
        $rendered_row['treasure_hunt']['status'] = 'completed';
      }
      else {
        $rendered_row['treasure_hunt']['status'] = 'pending';
      }
      $rendered_row['hints'] = $hints;
      $rows[] = $rendered_row;
    }

    return $this->serializer->serialize($rows, 'json');
  }

  public function searchArray($array, $element) {
    foreach ($array as $value) {
      if ($value['id'] == $element) {
        return true;
      }
    }
    return false;
  }

}
