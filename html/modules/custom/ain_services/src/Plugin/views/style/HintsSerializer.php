<?php

/**
 * @file
 * Contains \Drupal\ain_services\Plugin\views\style\HintsSerializer.
 */

namespace Drupal\ain_services\Plugin\views\style;

use Drupal\rest\Plugin\views\style\Serializer;

/**
 * The style plugin for serialized output formats.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "hints_serializer",
 *   title = @Translation("Hints Serializer"),
 *   help = @Translation("Serializes views row data using the HintsSerializer component."),
 *   display_types = {"data"}
 * )
 */
class HintsSerializer extends Serializer {

  /**
   * {@inheritdoc}
   */
  public function render() {
    $langcode = \Drupal::request()->query->get('langcode');
    if (!isset($langcode)) {
      $langcode = 'en';
    }
    $rows = [];
    $order = 1;

    foreach ($this->view->result as $row_index => $row) {
      $rendered_row = $this->view->rowPlugin->render($row);
      $rendered_row = ['order' => $order] + $rendered_row;
      $order++;
      $rows[] = $rendered_row;
    }

    return $this->serializer->serialize($rows, 'json');
  }

}
