<?php

namespace Drupal\ain_services\Plugin\rest\resource;

use Drupal\ain_services\Plugin\rest\resource\AinResponseResourceTrait;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;
use Drupal\node\Entity\Node;

/**
 * Extends.
 *
 * @RestResource(
 *   id = "ain_gamification_resource",
 *   label = @Translation("Ain Gamification"),
 *   uri_paths = {
 *     "canonical" = "/gamification/create",
 *     "https://www.drupal.org/link-relations/create" = "/gamification/create"
 *   }
 * )
 */
class AinGamificationResource extends ResourceBase {

  use AinResponseResourceTrait;

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
  array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition, $container->getParameter('serializer.formats'), $container->get('logger.factory')->get('rest'), $container->get('current_user')
    );
  }

  public static function getPoints($type) {
    $config = \Drupal::service('config.factory')->getEditable('ain_gamification.settings');
    return ($config->get('ain_gamification_' . $type)) ? $config->get('ain_gamification_' . $type) : 0;
  }

  public static function checkReferencedId($id, $type) {
    if (is_numeric($id)) {
      $node = Node::load($id);
      if ($node && $node->type->target_id == $type) {
        return true;
      }
    }
    return false;
  }

  /**
   * {@inheritdoc}
   */
  public function post(array $data) {

    //Allowed types to gain points from Mobile.
    $types = ['registration', 'share_app', 'plan_visit', 'share_content', 'treasure_hunt', 'hint', 'downloaded_game'];

    // Use current user after pass authentication to validate access.
    if (!$this->currentUser->hasPermission('restful post ain_gamification_resource')) {
      $message = 'You don\'t have permissions to access points.';
      return $this->ain_response('failed', $message, 403);
    }

    $error = false;
    $message = '';
    $type = isset($data['type']) ? $data['type'] : false;
    if($type && in_array($type, $types)) {
      $query = \Drupal::database()->select('gamification_field_data', 'points');
      $query->fields('points',['field_type', 'user_id']);
      $query->condition('points.field_type', $type);
      $query->condition('points.user_id', $this->currentUser->id());
      switch($type) {   
        case 'share_content':
        case 'share_app':
        case 'downloaded_game':
        case 'plan_visit':
          if (!isset($data['action_id'])) {
            $error = true;
            $message = 'You should add action_id';
            break;
          }
          $query->condition('points.action_id', $data['action_id']);
          $points = $this->getPoints($type);
          break;

        case 'registration':
          $points = $this->getPoints($type);
          break;

        case 'treasure_hunt':
          $points = (isset($data['points']) && is_numeric($data['points'])) ? $data['points'] : false;
          if(!isset($data['treasure_hunt']) || !$points) {
            $error = true;
            $message = 'You should add treasure_hunt and points';
            break;
          }
          if(!$this->checkReferencedId($data['treasure_hunt'], 'treasure_hunt')) {
            $error = true;
            $message = 'You should add a correct reference ID for treasure_hunt';
          }
          $query->condition('points.field_treasure_hunt', $data['treasure_hunt']);
          
          break;

        case 'hint':
          $points = (isset($data['points']) && is_numeric($data['points'])) ? $data['points'] : false;
          if (!isset($data['treasure_hunt']) || !isset($data['hint']) || !$points) {
            $error = true;
            $message = 'You should add treasure_hunt, points and hint';
            break;
          }
          if (!$this->checkReferencedId($data['treasure_hunt'], 'treasure_hunt') || !$this->checkReferencedId($data['hint'], 'hint')) {
            $error = true;
            $message = 'You should add a correct reference ID for treasure_hunt and hint';
          }
          $query->condition('points.field_treasure_hunt', $data['treasure_hunt']);
          $query->condition('points.field_hint', $data['hint']);
          break;
      }
    }
    else {
      $error = true;
      $message = 'You should add a vaild type';
    }

    if ($error) {
      return $this->ain_response('failed', $message, 422);
    }

    $has_points = $query->execute()->fetchField();

    if($has_points) {
      $message = 'User already have points for this type of points';
      if($type == 'hint') {
        $message = 'You’ve used this Clue already, please look for new clues';
      }
      return $this->ain_response('failed', $message, 422);
    }

    //Create new gamification entity.
    $user_id = $this->currentUser->id();
    $treasure_hunt_id = isset($data['treasure_hunt']) ? $data['treasure_hunt'] : [];
    $hint_id = isset($data['hint']) ? $data['hint'] : [];
    $action_id = isset($data['action_id']) ? $data['action_id'] : NULL;

    $gamification = entity_create('gamification', [
      'field_type' => $type,
      'field_point' => $points,
      'field_cuid' => $user_id,
      'user_id' => $user_id,
      'field_treasure_hunt' => $treasure_hunt_id,
      'field_hint' => $hint_id,
      'action_id' => $action_id,
      'status' => 1,
    ]);
    $gamification->save();

    // Check for successfully creation of gamification.
    if ($gamification) {
      $message = 'Points were created successfully.';
      return $this->ain_response('success', $message, 201, $gamification->id());
    }
    else {
      $message = 'An error occured, please try again.';
      return $this->ain_response('failed', $message, 422);
    }
  }
}
