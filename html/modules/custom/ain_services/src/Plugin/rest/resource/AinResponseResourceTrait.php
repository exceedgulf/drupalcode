<?php

namespace Drupal\ain_services\Plugin\rest\resource;

use Symfony\Component\HttpFoundation\JsonResponse;

require_once __DIR__ . '/../../../../includes/ain_api_utilities.inc';

/**
 * @internal
 * @todo Consider making public in https://www.drupal.org/node/2300677
 */
trait AinResponseResourceTrait {

  protected function ain_response($status = 'success', $message = '', $code = 200, $data = null) {
    $return = [
      'status' => $status,
      'messages' => [
        'en' => $message,
        'ar' => __translate_to_ar($message),
        'code' => $code
      ],
      'data' => $data
    ];
    return new JsonResponse($return, $code);
  }

  protected function ain_response_multiple($status = 'success', $messages = [], $code = 200, $data = null) {
    $return = [
      'status' => $status,
      'messages' => $messages,
      'data' => $data
    ];
    return new JsonResponse($return, $code);
  }

}
