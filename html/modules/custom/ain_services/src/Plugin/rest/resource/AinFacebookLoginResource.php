<?php

namespace Drupal\ain_services\Plugin\rest\resource;
use Drupal\simple_oauth\Repositories;
use Drupal\ain_services\Plugin\rest\resource\AinResponseResourceTrait;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;
use GuzzleHttp\Exception\RequestException;
use Drupal\user\Entity\User;
use League\OAuth2\Server\Entities\ClientEntityInterface;

/**
 * Extends.
 *
 * @RestResource(
 *   id = "ain_facebook_login_resource",
 *   label = @Translation("Facebook Login"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/facebook/login",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/facebook/login"
 *   }
 * )
 */
class AinFacebookLoginResource extends ResourceBase {

  use AinResponseResourceTrait;

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
  array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition, $container->getParameter('serializer.formats'), $container->get('logger.factory')->get('rest'), $container->get('current_user')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function post(array $data) {
    if(isset($data['facebook_token'])) {
      $client = \Drupal::httpClient();
      try {      
        $request = $client->request('GET', 'https://graph.facebook.com/me/?access_token=' . $data['facebook_token']);
        $status = $request->getStatusCode();
        if ($request->getStatusCode() == 200) {
          $response = json_decode($request->getBody());

          // Check if user exist on website.
          $query = \Drupal::database()->select('social_auth', 'social_auth');
          $query->fields('social_auth', ['user_id']);
          $query->condition('social_auth.provider_user_id', $response->id);
          $query->condition('social_auth.plugin_id', 'social_auth_facebook');
          $user_id = $query->execute()->fetchField();

          if($user_id) {
            $user = User::load($user_id);
            if($user) {
              // $collector = \Drupal::service('simple_oauth.repositories.access_token');
              // $token  = $collector->getNewToken($client_entity,[], $user_id);
              $message = 'Nice, valid facebook token, generate the access token now.';
              return $this->ain_response('success', $message, 200, $user_id);
            }  
          }
          $message = 'You don\'t have account connected to the current Facebook ID';
          return $this->ain_response('failed', $message, 422);
          
        }
      } catch (RequestException $e) {
        $message = 'Your account is experied please login using Facebook account again.';
        return $this->ain_response('failed', $message, 422);
      }   
    }
     
    $message = 'Facebook token missing.';
    return $this->ain_response('failed', $message, 422); 
  }
}
