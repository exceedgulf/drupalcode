<?php

namespace Drupal\ain_services\Plugin\rest\resource;

use Drupal\ain_services\Plugin\rest\resource\AinResponseResourceTrait;
use Drupal\webform\Entity\Webform;
use Drupal\webform\WebformSubmissionForm;
use Drupal\webform_rest\Plugin\rest\resource\WebformSubmitResource;

require_once __DIR__ . '/../../../../includes/ain_api_utilities.inc';

/**
 * Extends.
 *
 * @RestResource(
 *   id = "ain_webform_resource",
 *   label = @Translation("Ain Webform"),
 *   uri_paths = {
 *     "canonical" = "/webform_rest/submit",
 *     "https://www.drupal.org/link-relations/create" = "/webform_rest/submit"
 *   }
 * )
 */
class AinWebformResource extends WebformSubmitResource {

  use AinResponseResourceTrait;

  /**
   * {@inheritdoc}
   */
  public function post(array $webform_data) {

    // Basic check for webform ID.
    if (empty($webform_data['webform_id'])) {
      $message = 'Failed';
      return $this->ain_response('failed', $message, 500);
    }

    // Convert to webform values format.
    $values = [
      'webform_id' => $webform_data['webform_id'],
      'entity_type' => NULL,
      'entity_id' => NULL,
      'in_draft' => FALSE,
      'uri' => '/webform/' . $webform_data['webform_id'] . '/api',
    ];

    // Don't submit webform ID.
    unset($webform_data['webform_id']);

    $values['data'] = $webform_data;

    // Check for a valid webform.
    $webform = Webform::load($values['webform_id']);
    if (!$webform) {
      $message = 'Failed: Invalid webform_id value.';
      return $this->ain_response('failed', $message, 422);
    }

    // Check webform is open.
    $is_open = WebformSubmissionForm::isOpen($webform);

    if ($is_open === TRUE) {
      // Validate submission.
      $errors = WebformSubmissionForm::validateFormValues($values);

      // Check there are no validation errors.
      if (!empty($errors)) {
        $errors_ar = $errors;
        foreach ($errors_ar as $key => &$value) {
          $value = __translate_to_ar($value);
        }

        $messages = [
          'en' => $errors,
          'ar' => $errors_ar,
        ];
        return $this->ain_response_multiple('failed', $messages, 422);
      }
      else {
        // Return submission ID.
        $webform_submission = WebformSubmissionForm::submitFormValues($values);
        $message = 'Success';
        return $this->ain_response('success', $message, 200, $webform_submission->id());
      }
    }
  }

}
