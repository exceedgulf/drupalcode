<?php

namespace Drupal\ain_services\Plugin\rest\resource;

use Drupal\ain_services\Plugin\rest\resource\AinResponseResourceTrait;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;

/**
 * Extends.
 *
 * @RestResource(
 *   id = "ain_facebook_register_resource",
 *   label = @Translation("Facebook Register"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/facebook/register",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/facebook/register"
 *   }
 * )
 */
class AinFacebookRegisterResource extends ResourceBase {

  use AinResponseResourceTrait;

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
  array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition, $container->getParameter('serializer.formats'), $container->get('logger.factory')->get('rest'), $container->get('current_user')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function post(array $data) {
    $message = 'Under Development';
    return $this->ain_response('failed', $message, 422); 
  }
}
