<?php

namespace Drupal\ain_services\Plugin\rest\resource;

use Drupal\rest\Plugin\rest\resource\EntityResourceValidationTrait;
use Drupal\Component\Render\PlainTextOutput;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * @internal
 * @todo Consider making public in https://www.drupal.org/node/2300677
 */
trait AinEntityResourceValidationTrait {

  use EntityResourceValidationTrait;

  /**
   * Verifies that the whole entity does not violate any validation constraints.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to validate.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException
   *   If validation errors are found.
   */
  protected function validate(EntityInterface $entity, array $fields_to_validate = []) {
    $messages = [];
    // @todo Remove when https://www.drupal.org/node/2164373 is committed.
    if (!$entity instanceof FieldableEntityInterface) {
      return;
    }
    $violations = $entity->validate();



    // Remove violations of inaccessible fields as they cannot stem from our
    // changes.
    $violations->filterByFieldAccess();

    if ($violations->count() > 0) {
      foreach ($violations as $violation) {
        // We strip every HTML from the error message to have a nicer to read
        // message on REST responses.
        switch ($violation->getPropertyPath()) {
          case 'mail':
            $messages['mail']['en'] = 'The Email ' . $entity->getEmail() . ' is already taken';
            $messages['mail']['ar'] = 'البريد الإلكتروني ' . $entity->getEmail() . ' مستخدم مسبقاً';
            break;
          case 'name':
            $messages['name']['en'] = 'Mobile number ' . $entity->getUsername() . ' is already taken';
            $messages['name']['ar'] = 'رقم الهاتف ' . $entity->getUsername() . ' مستخدم مسبقاً';
            break;
          default:
            $messages[$violation->getPropertyPath()]['en'] = PlainTextOutput::renderFromHtml($violation->getMessage());
            $messages[$violation->getPropertyPath()]['ar'] = PlainTextOutput::renderFromHtml($violation->getMessage());
        }
      }

      return $messages;
    }
  }

}
