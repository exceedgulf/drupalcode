<?php

namespace Drupal\ain_services\Repositories;

use Drupal\user\UserAuthInterface;
use Drupal\user\Entity\User;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use Drupal\simple_oauth\Entities\UserEntity;
use GuzzleHttp\Exception\RequestException;

class UserFacebookRepository implements UserRepositoryInterface {

  /**
   * @var \Drupal\user\UserAuthInterface
   */
  protected $userAuth;

  /**
   * UserRepository constructor.
   *
   * @param \Drupal\user\UserAuthInterface $user_auth
   *   The service to check the user authentication.
   */
  public function __construct(UserAuthInterface $user_auth) {
    $this->userAuth = $user_auth;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserEntityByUserCredentials($facebook_token, $password, $grantType, ClientEntityInterface $clientEntity) {
    $UserEntity = new UserEntity();
    $client = \Drupal::httpClient();

    try {
      $request = $client->request('GET', 'https://graph.facebook.com/me/?access_token=' . $facebook_token);
      $status = $request->getStatusCode();
      if ($request->getStatusCode() == 200) {
        $response = json_decode($request->getBody());

          // Check if user exist on website.
        $query = \Drupal::database()->select('social_auth', 'social_auth');
        $query->fields('social_auth', ['user_id']);
        $query->condition('social_auth.provider_user_id', $response->id);
        $query->condition('social_auth.plugin_id', 'social_auth_facebook');
        $user_id = $query->execute()->fetchField();

        if ($user_id) {
          $user = User::load($user_id);
          if ($user) {
            //TOTO check if user status is avalable.
            $UserEntity->setIdentifier($user_id);
            return $UserEntity;
          }
        }
        return null;

      }
    } catch (RequestException $e) {
      return null;
    }  

    
  }

}
