# Seeds Toolbar
![Seeds Toolbar](https://www.drupal.org/files/ezgif-2-bf7cfbfa276e.gif)
Is a new UX concept for Drupal admin navigation.

Seeds Toolbar is part of our journey to build [Seeds](https://drupal.org/project/seeds), a kickoff distribution for SMEs.

## Seeds Toolbar Options
* Choose between light or dark mode
* Change Drupal logo to your website logo
* Add your helpdesk link and leave it empty to hide it.
* Load custom css style.

## Seeds Toolbar Features

### UX enhancements
 We invested a lot of time enhancing the experience of daily tasks on Drupal CMS. 

### Mobile first:
Editing your website from mobile is fully supported, indeed it's the best experience that will never stop enhanced. 

### RTL compatibility
 We developed more than 55 Arabic websites, and we suffer a lot from lack of support for RTL interfaces, Root is your best choice to get rid of heavy-duty website updates. 

### FREEDOM
Seeds Toolbar is simple, colorless and can be used with any identity, you can choose between light or dark mode.

### Support
 Your request is ours, and this theme is developed to serve more than 70 clients websites crafted by our developers at [sprintive](https://www.sprintive.com).

## This module is tested with the following modules

* Responsive Preview
* Coffee
* Devel
* Block Content Permissions
* Masquerade
* Menu Link Attributes
* Tour UI
* Acquia connector

![Sprintive Pattern](https://www.drupal.org/files/serpartor-sprintive.png)

### This module is sponsored by:
![Sprintive](https://www.drupal.org/files/styles/grid-3/public/drupal_3.png)