<?php

namespace Drupal\seeds_toolbar;

use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Helper function for SeedsToolbar.
 */
class SeedsManager {

  /**
   * Language manager
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * Current user
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * Entity type manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Indicates if the "block_content_permissions" module exists.
   *
   * @var boolean
   */
  protected $blockContentPermissionsModule;

  /**
   * Module handler
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $module_handler;

  /**
   * Constructor.
   */
  public function __construct($language_manager, $current_user, $entity_type_manager, $module_handler) {
    $this->languageManager = $language_manager;
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->module_handler = $module_handler;
    $this->blockContentPermissionsModule = $module_handler->moduleExists('block_content_permissions');
  }

  /**
   * Build "Add" menu item.
   */
  public function buildMenu($entity_type, $route) {
    // Seeds toolbar should not depend on any entity type, se we check if the entity type exists or not.
    if ($this->entityTypeManager->hasDefinition($entity_type)) {
      $entities = $this->entityTypeManager->getStorage($entity_type)->loadMultiple();
    } else {
      return [];
    }
    $items = [];
    foreach ($entities as $entity) {
      $id = $entity->id();
      $url = Url::fromRoute($route, [$entity_type => $id]);
      // Block content are a special case, so we want to handle it.
      if ($entity_type == 'block_content_type') {
        $access = $this->handleBlockPermission($entity);
      } else {
        $access = $this->userHasAccess($url);
      }
      if ($access) {
        $items[$id] = Link::fromTextAndUrl($entity->label(), $url);

      }
    }
    if (empty($items)) {
      return [];
    } else {
      return [
        '#menu_name' => $entity_type . '-add-menu',
        '#items' => $items,
        '#theme' => 'seeds_add_menu',
      ];
    }
  }

  /**
   * Check if the user has access to internal link.
   */
  private function userHasAccess($url) {
    return $url->access($this->currentUser);
  }

  /**
   * Check if block_content_permission enabled.
   */
  private function handleBlockPermission($block_type) {
    if ($this->blockContentPermissionsModule) {
      $type_id = $block_type->id();
      return $this->currentUser->hasPermission("create $type_id block content");
    } else {
      return $this->currentUser->hasPermission("administer blocks");
    }
  }

  /**
   * Get current language direction.
   */
  public function getDirection() {
    $dir = $this->languageManager->getCurrentLanguage()->getDirection();
    return ($dir == 'ltr') ? 'left' : 'right';
  }

  /**
   * Sort main toolbar links.
   */
  public static function sortTabs(array $items, array $first_tabs) {
    $counter = 1;
    foreach ($items as $id => &$item) {
      if (in_array($id, $first_tabs)) {
        $item['#weight'] = array_search($id, $first_tabs);
      } elseif ($id == 'admin_toolbar_tools') {
        // Always put admin_toolbar_tools at the last tab.
        $item['#weight'] = 1000;
      } else {
        $item['#weight'] = count($first_tabs) + $counter;
      }
      $counter++;
    }
    return $items;
  }

}
