<?php

namespace Drupal\socialfeed\Services;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class FacebookPostCollectorFactory.
 *
 * @package Drupal\socialfeed
 */
class FacebookPostCollectorFactory {

  /**
   * Default Facebook application id.
   *
   * @var string
   */
  protected $defaultAppId;

  /**
   * Default Facebook application secret.
   *
   * @var string
   */
  protected $defaultAppSecret;

  /**
   * Page name.
   *
   * @var string
   */
  protected $pageName;

  /**
   * FacebookPostCollector constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $config = $configFactory->get('socialfeed.facebooksettings');
    $this->defaultAppId = $config->get('app_id');
    $this->defaultAppSecret = $config->get('secret_key');
    $this->defaultUserToken = $config->get('user_token');
    $this->pageName = $config->get('page_name');
  }

  /**
   * Creates a pre-configured instance.
   *
   * @param string $appId
   *   The facebook application id.
   * @param string $appSecret
   *   The facebook application secret.
   * @param string $userToken
   *   The facebook user token.
   * @param string $pageName
   *   The facebook page name.
   *
   * @return \Drupal\socialfeed\Services\FacebookPostCollector
   *   A fully configured instance from FacebookPostCollector.
   *
   * @throws \Exception
   *   If the instance cannot be created, such as if the ID is invalid.
   */
  public function createInstance(string $appId, string $appSecret, string $userToken, string $pageName) {
    return new FacebookPostCollector(
      $appId ?: $this->defaultAppId,
      $appSecret ?: $this->defaultAppSecret,
      $userToken ?: $this->defaultUserToken,
      $pageName ?: $this->pageName = $pageName
    );
  }

}
