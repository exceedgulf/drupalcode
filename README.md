### Seeds: Kickoff distribution for SMEs

[![Latest Stable Version](https://poser.pugx.org/sprintive/seeds-project/v/stable)](https://packagist.org/packages/sprintive/seeds-project) [![Total Downloads](https://poser.pugx.org/sprintive/seeds-project/downloads)](https://packagist.org/packages/sprintive/seeds-project) [![Latest Unstable Version](https://poser.pugx.org/sprintive/seeds-project/v/unstable)](https://packagist.org/packages/sprintive/seeds-project) [![License](https://poser.pugx.org/sprintive/seeds-project/license)](https://packagist.org/packages/sprintive/seeds-project) [![composer.lock](https://poser.pugx.org/sprintive/seeds-project/composerlock)](https://packagist.org/packages/sprintive/seeds-project)

[![Seeds](https://www.drupal.org/files/styles/grid-3/public/project-images/Sprintive%20Seets-01.png)](https://www.drupal.org/project/seeds)

Light distribution to kick off all projects regardless scale, you can use it to speed up your projects.

Seeds focusing on Arabic website with RTL interfaces so if you have any issue with your Arabic language website you are more than welcome to contribute with us.


#### Sponsored and developed by:

[![Sprintive](https://www.drupal.org/files/styles/grid-3/public/sprintive-drupal.png?itok=EwOUBjIZ)](http://sprintive.com)

Sprintive is a web solution provider which transform ideas into realities, where humans are the center of everything, and Drupal is the heart of our actions, it has built and delivered Drupal projects focusing on a deep understanding of business goals and objective to help companies innovate and grow.
